import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomeComponent} from "./components/home/home.component";
import {CatalogComponent} from "./components/catalog/catalog.component";
import {ShoppingCartComponent} from "./components/shopping-cart/shopping-cart.component";
import {SingleCategoryComponent} from "./components/single-category/single-category.component";
import {OrdersComponent} from "./components/orders/orders.component";

const routes: Routes = [
  {path: "", component: HomeComponent},
  {path: "catalog", component: CatalogComponent},
  {path: "shopping-cart", component: ShoppingCartComponent},
  {path: 'catalog/:category',component: SingleCategoryComponent},
  {path: 'orders', component: OrdersComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
