import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {CategoriesService} from "../../service/categories.service";
import { Router} from "@angular/router";

@Component({
  selector: 'app-catalog-list',
  templateUrl: './catalog-list.component.html',
  styleUrls: ['./catalog-list.component.scss']
})
export class CatalogListComponent implements OnInit {

  constructor(public categoriesService: CategoriesService, private  route: Router) { }

  @Input() dataCategory: any[] = []


  menu = [
    "https://catherineasquithgallery.com/uploads/posts/2021-02/1613566938_217-p-fon-dlya-prezentatsii-po-informatike-245.jpg",
    "https://miro.medium.com/max/1400/0*fZWhdJovVSJ6T5f7.jpeg",
    "https://previews.123rf.com/images/serezniy/serezniy1809/serezniy180942478/108760815-set-of-stylish-male-clothes-on-wooden-background.jpg",
    "https://previews.123rf.com/images/belchonock/belchonock1802/belchonock180292505/96250535-women-clothing-on-bed-background.jpg"
  ]


  ngOnInit(): void {
    this.categoriesService.getCategories()
    console.log(this.categoriesService.categories)
  }

  getSingleCategory(category: string) {
    this.categoriesService.getSinlgeCategory(category)
    this.route.navigate(['catalog',category])
  }






}
