import {Component, DoCheck, OnInit} from '@angular/core';
import {ShoppingCartService} from "../../service/shopping-cart.service";

@Component({
  selector: 'app-items-in-cart',
  templateUrl: './items-in-cart.component.html',
  styleUrls: ['./items-in-cart.component.scss']
})
export class ItemsInCartComponent implements OnInit,DoCheck {

  constructor(private  shoppingCartService: ShoppingCartService) { }
  quantity = 0

  ngOnInit(): void {
    this.quantity =  this.shoppingCartService.shoppingCart.reduce((acc,item) => {return acc+item.count},0)
  }

  ngDoCheck() {
   this.quantity = this.shoppingCartService.shoppingCart.reduce((acc,item) => {return acc+item.count},0)
  }

}
