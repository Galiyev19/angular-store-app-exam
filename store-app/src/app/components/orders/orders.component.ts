import {Component, Input, OnInit} from '@angular/core';
import {OrdersService} from "../../service/orders.service";

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {

  constructor(private  ordersService: OrdersService) { }

  @Input() orders : any

  ngOnInit(): void {
    this.ordersService.getOrders()
    this.orders = this.ordersService.orders

  }

}
