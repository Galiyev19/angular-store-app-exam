import {Component, Input, OnInit} from '@angular/core';
import {ProductService} from "../../service/product.service";
import {Product} from "../../model/model";
import {ShoppingCartService} from "../../service/shopping-cart.service";


@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  constructor(public productService: ProductService, private shoppingCartService: ShoppingCartService) {
  }

  public products: Product[] = []

  ngOnInit(): void {
    this.productService.getAllProducts()
    // console.log(this.productService.products)
  }

  addShoppingCart(id: number) {
    this.shoppingCartService.addToShoppingCart(id)
    console.log(this.shoppingCartService.shoppingCart)
  }

}
