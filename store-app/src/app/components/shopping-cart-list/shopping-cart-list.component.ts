import {ChangeDetectorRef, Component, DoCheck, Input, OnInit} from '@angular/core';
import {ShoppingCartService} from "../../service/shopping-cart.service";

@Component({
  selector: 'app-shopping-cart-list',
  templateUrl: './shopping-cart-list.component.html',
  styleUrls: ['./shopping-cart-list.component.scss']
})
export class ShoppingCartListComponent implements OnInit,DoCheck {

  constructor(public shoppingCartService: ShoppingCartService, private cdr: ChangeDetectorRef) { }
  @Input() shoppingCart: any[] = []

  // quantity = 0

  ngOnInit(): void {
    this.shoppingCart = this.shoppingCartService.shoppingCart
    // this.quantity = this.shoppingCartService.shoppingCart.length
    console.log(this.shoppingCartService.shoppingCart)
  }

  private length = this.shoppingCartService.shoppingCart.length

  ngDoCheck() {
    if(this.length !== this.shoppingCartService.shoppingCart.length){
      this.cdr.markForCheck()
      this.length = this.shoppingCartService.shoppingCart.length
    }
  }


  increaseCount(id: number) {
    this.shoppingCartService.increaseCount(id)
  }

  decreaseCount(id: number){
    this.shoppingCartService.decreaseCount(id)
  }

  deleteItem(id: number) {
    this.shoppingCartService.deleteItem(id)
  }

}
