import {Component, DoCheck, Input, OnInit} from '@angular/core';
import {ShoppingCartService} from "../../service/shopping-cart.service";
import {OrdersService} from "../../service/orders.service";

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.scss']
})
export class ShoppingCartComponent implements OnInit,DoCheck {

  constructor(private shoppingCartService: ShoppingCartService, private ordersService: OrdersService) { }

  @Input() shoppingCart: any[] = []

  show = false;

  //Кол-во товаров в корзине
  quantity = 0
  //Общая сумма заказа
  total = 0
  //Есть ли в корзине что-то
  length = 0

  ngOnInit(): void {
    this.shoppingCart = this.shoppingCartService.shoppingCart
  }

  ngDoCheck(): void {
    this.quantity = this.shoppingCartService.shoppingCart.reduce((acc,item) => {return acc+item.count},0)
    this.total = this.shoppingCartService.shoppingCart.reduce((acc,item) => acc + item.total,0)
    this.length = this.shoppingCartService.shoppingCart.length
  }

  //Заказать
  order() {
    this.ordersService.temp = this.shoppingCartService.shoppingCart;
    this.show = true
    this.shoppingCartService.clearShoppingCart()
  }




}
