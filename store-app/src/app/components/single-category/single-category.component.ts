import {ChangeDetectorRef, Component, DoCheck, OnInit} from '@angular/core';
import {CategoriesService} from "../../service/categories.service";
import {ShoppingCartService} from "../../service/shopping-cart.service";

@Component({
  selector: 'app-single-category',
  templateUrl: './single-category.component.html',
  styleUrls: ['./single-category.component.scss']
})
export class SingleCategoryComponent implements OnInit, DoCheck {

  constructor(private categoriesService: CategoriesService,private cdr: ChangeDetectorRef,private shoppingCartService: ShoppingCartService) { }

  data: any
  ngOnInit(): void {
    this.data = this.categoriesService.singleCategoryData
    console.log(this.data)
  }

  ngDoCheck() {
    if(this.data !== this.categoriesService.singleCategoryData){
      this.cdr.markForCheck()
      this.data = this.categoriesService.singleCategoryData
    }
  }

  addShoppingCart(id: number) {
    this.shoppingCartService.addToShoppingCart(id)
    console.log(this.shoppingCartService.shoppingCart)
  }

}
