import { Injectable } from '@angular/core';
import {HttpClient,HttpParams} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {

  constructor(private httpClient: HttpClient) { }

  url = 'https://fakestoreapi.com/products/categories'


  categories: any;
  singleCategoryData: any;

  getCategories() {
    return this.httpClient.get(this.url).subscribe(response => {
      this.categories = response
    })
  }


  getSinlgeCategory(category: string) {
    return this.httpClient.get('https://fakestoreapi.com/products/category' + `/${category}`, {
      params: new HttpParams({})
    }).subscribe(res => this.singleCategoryData = res)
  }


}
