import { Injectable } from '@angular/core';
import * as uuid from 'uuid'

@Injectable({
  providedIn: 'root'
})
export class OrdersService  {

  constructor() { }


  temp: any[] = [];

  orders: any[] = [];

  getOrders() {

    let date = new Date()
    let totalSum = this.temp.reduce((acc,item) => acc + item.total,0)

    if(this.temp.length !== 0) {

      let obj = {
        id: uuid.v4(),
        date: date.toLocaleDateString(),
        orderList: this.temp,
        totalSumOrder: totalSum
      }
      this.orders.push(obj)

      // console.log(this.temp)
      console.log(this.orders)
    }

  }
}
