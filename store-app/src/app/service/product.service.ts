import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Product} from "../model/model";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private  httpClient: HttpClient) { }

  url = 'https://fakestoreapi.com/products'

  public  products: Product[] = []

  public getAllProducts () :Observable<Product[]> {
    // @ts-ignore
    return this.httpClient.get<Product[]>(this.url).subscribe((response) => {this.products = response})
  }
}
