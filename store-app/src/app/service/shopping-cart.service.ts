import {Injectable} from '@angular/core';
import {ProductService} from "./product.service";


@Injectable({
  providedIn: 'root'
})
export class ShoppingCartService {

  constructor(private productService: ProductService) {
  }

  public shoppingCart: any[] = []

  //Добавление в корзину
  addToShoppingCart(id: number) {

    const idx = this.productService.products.findIndex(item => item.id === id)

    if(!this.shoppingCart[idx]){
      const obj = {
        id: idx,
        products: this.productService.products[idx],
        count: 1,
        total: this.productService.products[idx].price,
      }
      this.shoppingCart.push(obj)
    }else{
      return
    }
  }

  //Увеличить кол-во товаров в корзине
  increaseCount(id: number){
    const idx = this.shoppingCart.findIndex(item => item.id === id)
    this.shoppingCart[idx].count += 1
    this.shoppingCart[idx].total = this.shoppingCart[idx].count * this.shoppingCart[idx].products.price
  }

  //Уменшить кол-во товаров в корзине
  decreaseCount(id: number){
    const idx = this.shoppingCart.findIndex(item => item.id === id)
    if(this.shoppingCart[idx].count !== 1){
      this.shoppingCart[idx].count -= 1
      this.shoppingCart[idx].total  -= this.shoppingCart[idx].products.price
    }else{
      return
    }
  }

  //Удалить из корзины товар
  deleteItem(id:number){
    const idx = this.shoppingCart.findIndex(item => item.id === id)
    // this.shoppingCart.filter(item => item.id !== id)
     this.shoppingCart =  [...this.shoppingCart.slice(0, idx), ... this.shoppingCart.slice(idx + 1)]
  }

  //Для очищение корзины при нажатие заказать
  clearShoppingCart() {
    this.shoppingCart = []
  }
}
